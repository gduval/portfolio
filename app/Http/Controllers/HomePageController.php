<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\View\View;

class HomePageController extends Controller
{
    public function index(): View
    {
        $projects = Project::latest()->where('draft', false)->get();
        return view('home.home', compact('projects'));
    }

    public function aboutMe(): View
    {
        return view('about-me.about-me');
    }
}
