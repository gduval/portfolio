<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProjectRequest;
use App\Http\Requests\UpdateProjectRequest;
use App\Models\Project;
use App\Repositories\ProjectRepository;
use ErrorException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;
use RuntimeException;

class ProjectController extends Controller
{
    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    public function __construct(ProjectRepository $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('project.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreProjectRequest $request
     * @return RedirectResponse
     */
    public function store(StoreProjectRequest $request): RedirectResponse
    {
        if (!$request->hasFile('logo') || !$request->hasFile('cover')) {
            throw new RuntimeException('Image upload failed.');
        }

        $validated = $request->validated();

        $validated['cover'] = '/storage/' . $request->file('cover')->store('images', 'public');
        $validated['logo'] = '/storage/' . $request->file('logo')->store('images', 'public');

        Project::create($validated);

        return redirect()->route('admin-projects');
    }

    /**
     * Display the specified resource.
     *
     * @param Project $project
     * @return Factory|View
     */
    public function show(Project $project)
    {
        return view('project.show', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Project $project
     * @return Factory|View
     */
    public function edit(Project $project)
    {
        return view('project.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Project $project
     * @param UpdateProjectRequest $request
     * @return RedirectResponse
     */
    public function update(Project $project, UpdateProjectRequest $request): RedirectResponse
    {
        // Truncate Froala Footer from content
        $validated = $request->validated();

        if ($request->hasFile('cover')) {
            $cover = '/storage/' . $request->file('cover')->store('images', 'public');
        } else {
            $cover = $project->getCover();
        }

        if ($request->hasFile('logo')) {
            $logo = '/storage/' . $request->file('logo')->store('images', 'public');
        } else {
            $logo = $project->getLogo();
        }

        $validated['cover'] = $cover;
        $validated['logo'] = $logo;
        $validated['draft'] = $request->get('draft') ?? false;

        $project->update($validated);

        return redirect()->route('admin-projects')->with(['success' => 'Project successfully created.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Project $project
     * @return JsonResponse
     * @throws ErrorException
     */
    public function destroy(Project $project)
    {
        $this->projectRepository->remove($project);

        return new JsonResponse('Project successfully removed.', 204);
    }
}
