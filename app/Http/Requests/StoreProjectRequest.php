<?php

namespace App\Http\Requests;

use App\Models\Project;
use App\Services\ProjectService;
use Illuminate\Foundation\Http\FormRequest;

class StoreProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'content' => ProjectService::truncateFroalaFooter($this->get('content'))
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:5|max:50',
            'content' => 'required|min:50',
            'type' => 'required',
            'cover' => 'required|mimes:jpeg,png',
            'logo' => 'required|mimes:jpeg,png,svg',
        ];
    }
}
