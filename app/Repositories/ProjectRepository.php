<?php


namespace App\Repositories;


use App\Models\Project;

class ProjectRepository
{
    /**
     * @param Project $project
     * @return bool
     */
    public function save(Project $project): bool
    {
        return $project->save();
    }

    /**
     * @param string $title
     * @param string $content
     * @param string $type
     * @param bool $draft
     * @param string $cover
     * @param string $logo
     * @param Project $project
     * @return bool
     */
    public function update(string $title, string $content, string $type, bool $draft, string $cover, string $logo, \App\Models\Project $project): bool
    {
        $project->setTitle($title);
        $project->setContent($content);
        $project->setType($type);
        $project->setDraft($draft);
        $project->setCover($cover);
        $project->setLogo($logo);

        return $project->save();
    }

    /**
     * @param Project $project
     * @return bool
     * @throws \ErrorException
     */
    public function remove(Project $project): bool
    {
        try {
            return $project->delete();
        } catch (\Exception $e) {
            throw new \ErrorException(500, $e->getMessage());
        }
    }
}
