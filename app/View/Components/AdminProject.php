<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AdminProject extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.admin-project');
    }

    public function projects()
    {
        return \App\Models\Project::latest()->get();
    }
}
