<?php


namespace App\Services;


use App\Models\Project;
use App\Repositories\ProjectRepository;

class ProjectService
{
    private const FROALA_CREDIT_TEXT_LENGTH = 229;

    public static function truncateFroalaFooter(string $content) {
        return substr($content, 0, ((self::FROALA_CREDIT_TEXT_LENGTH) * -1));
    }

}
