<?php

use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', static function () {
    return redirect()->route('home');
})->name('home');
Route::get(RouteServiceProvider::HOME, [\App\Http\Controllers\HomePageController::class, 'index'])->name('home');
Route::get('/about-me', [\App\Http\Controllers\HomePageController::class, 'aboutMe'])->name('about-me');

Route::get('/projects/{project:slug}', [\App\Http\Controllers\ProjectController::class, 'show'])->name('projects.show');
Route::get('/projects/edit/{project:slug}', [\App\Http\Controllers\ProjectController::class, 'edit'])->name('projects.edit');
Route::get('/projects', [\App\Http\Controllers\ProjectController::class, 'create'])->name('projects.create');
Route::post('/projects', [\App\Http\Controllers\ProjectController::class, 'store'])->name('projects.store');
Route::put('/projects/{project:slug}', [\App\Http\Controllers\ProjectController::class, 'update'])->name('projects.update');
Route::delete('/projects/{project:slug}', [\App\Http\Controllers\ProjectController::class, 'destroy'])->name('projects.destroy');

//Admin
Route::get('/admin', static function () {
    return redirect()->route('admin');
});
Route::get('/admin/home', [\App\Http\Controllers\AdminController::class, 'index'])->name('admin');
Route::get('/admin/projects', [\App\Http\Controllers\AdminController::class, 'showProjects'])->name('admin-projects');

//Auth
Auth::routes(['register' => false]);
Route::get('/logout', [\App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');
