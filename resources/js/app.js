/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

"use strict";

const TARGET_BLANK = "_blank"

document.addEventListener('DOMContentLoaded', () => {

    let mouseX = window.innerWidth / 2;
    let mouseY = window.innerHeight / 2;
    let windowHovered = false;

    let circle = {
        el: document.getElementById('circle'),
        x: window.innerWidth / 2,
        y: window.innerHeight / 2,
        w: 80,
        h: 80,

        update: function () {
            let l = this.x - this.w / 2;
            let t = this.y - this.h / 2;

            this.el.style.transform = `translate3d(${l}px, ${t}px, 0)`;
        }
    }

    function displayCursor() {
        circle.el.style.opacity = "1";
    }

    window.addEventListener('mousemove', (event) => {

        if (!windowHovered) {
            displayCursor()
            windowHovered = true;
        }

        mouseX = event.clientX;
        mouseY = event.clientY;

        move();
    })

    window.addEventListener('mouseover', (event) => {
        morphCursor(event.target.getAttribute('data-hover-style'), event);
    })

    function morphCursor(hoverStyle, event) {
        let parentNode

        if (!hoverStyle) {
            parentNode = event.target.closest('*[data-hover-style]')
            if (parentNode) {
                morphCursor(parentNode.getAttribute('data-hover-style'), event)
            } else {
                circle.el.setAttribute('class', '')
            }
        } else if (hoverStyle === "text-small") {
            circle.el.setAttribute('class', 'text-cursor-small')
        } else if (hoverStyle === "text") {
            circle.el.setAttribute('class', 'text-cursor')
        } else if (hoverStyle === "dot") {
            circle.el.setAttribute('class', 'dot-cursor')
        }
    }

    // setInterval (move,1000/60)

    function move() {
        circle.x = lerp(circle.x, mouseX, .7);
        circle.y = lerp(circle.y, mouseY, .7);
        circle.update()
    }

    function lerp(start, end, amt) {
        return (1 - amt) * start + amt * end
    }

    // Boot Animation
    function overlayOpen() {
        let whiteOverlay = document.getElementById('white-overlay');

        if (whiteOverlay)
            whiteOverlay.style.width = "0px"
    }

    function overlayClose() {
        let whiteOverlay = document.getElementById('white-overlay');

        if (!whiteOverlay)
            return

        whiteOverlay.style.left = "0"
        whiteOverlay.style.width = "100%"
    }

    window.addEventListener('click', (event) => {
        let link;
        let href;
        let eventTarget = event.target;

        if (eventTarget.nodeName === 'A') {
            link = eventTarget
        } else {
            link = eventTarget.closest('a')
        }

        if (!link) {
            return
        }

        href = link.getAttribute('href')

        if (!href || href.startsWith('#') || (href && (href.includes('mailto') || link.getAttribute('target') === TARGET_BLANK)))
            return

        event.preventDefault()

        overlayClose()

        setTimeout(function () {
            window.location.href = href;
        }, 1000);
    })

    overlayOpen()
})
