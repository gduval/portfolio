document.addEventListener('DOMContentLoaded', () => {

    const responsiveNavButton = document.getElementById('responsive-nav-button');
    const navCircleBackground = document.getElementById('nav-circle-background')
    const navContainer = document.getElementById('responsive-nav-container')
    const navSeparator = document.getElementById('responsive-nav-separator')
    const navItems = document.getElementsByClassName('responsive-nav-item')
    const body = document.getElementsByTagName('body')[0]
    const html = document.getElementsByTagName('html')[0]

    let circleSize;

    let navSeparatorTimeOut;
    let itemsListTimeOut;
    let navItemsTimeOut = [];

    responsiveNavButton.addEventListener('click', () => {
        navCircleBackground.classList.toggle('circle-open')

        body.classList.toggle('overflow-hidden')
        html.classList.toggle('overflow-hidden')

        displayNavContainer()

        if (!navCircleBackground.classList.contains('circle-open'))
            closeMobileNav()
        else
            openMobileNav()

        navCircleBackground.style.width = circleSize + "px"
        navCircleBackground.style.height = circleSize + "px"
    })

    function closeMobileNav() {
        circleSize = 0

        navSeparator.classList.remove('nav-separator-open')

        hideNavContainer()

        Array.from(navItems).forEach((navItem) => {
            navItem.classList.remove('nav-open')
        })

        clearTimeout(navSeparatorTimeOut)
        clearTimeout(itemsListTimeOut)

        navItemsTimeOut.forEach((timeOut) => {
            clearTimeout(timeOut)
        })

        navItemsTimeOut = []
    }

    function openMobileNav() {
        circleSize = (window.innerHeight >= window.innerWidth ? window.innerHeight : window.innerWidth) * 2.5

        navSeparatorTimeOut = setTimeout(() => {
            navSeparator.classList.add('nav-separator-open')
        }, 1000)

        itemsListTimeOut = setTimeout(() => {
            Array.from(navItems).forEach((navItem, index) => {
                navItemsTimeOut.push(setTimeout(() => {
                    navItem.classList.add('nav-open')
                }, index * 300))
            })
        }, 400)
    }

    function hideNavContainer() {
        navContainer.style.opacity = '0';
        navContainer.style.visibility = 'hidden';
    }

    function displayNavContainer() {
        navContainer.style.opacity = '1';
        navContainer.style.visibility = 'visible';
    }
})
