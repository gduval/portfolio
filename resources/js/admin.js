require('./bootstrap');

window.Vue = require('vue');

export const app = new Vue({
    el: '#app',
    data: {
        project: {
            type: 'test'
        }
    },
    methods: {
        updateHiddenCategories: function () {
            $('#categories').val(
                $('#categories_tagsinput').val()
            );
        },
        deleteArticle: function (deleteUrl) {
            if (!deleteUrl) {
                return;
            }

            if (confirm('You are gonna delete this project. Continue ?')) {
                $.ajax({
                    url: deleteUrl,
                    method: 'delete',
                    data: {
                        '_token': $('meta[name="csrf-token"]').attr('content')
                    }
                }).done(function () {
                    location.reload();
                });
            }
        }
    }
});

// Froala editor
$(document).ready(function () {
    new FroalaEditor('#editor', {
        imageUploadURL: '/froala-editor/upload-image.php',
        imageUploadParams: {
            id: 'my_editor'
        }
    })
});
