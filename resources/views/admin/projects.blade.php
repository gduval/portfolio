@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <a class="btn btn-outline-dark mt-5" href="{{ route('projects.create') }}" role="button">Create
                    Project</a>
            </div>
        </div>
        <div class="row py-5">
            <x-admin-project/>
        </div>
    </div>
@endsection
