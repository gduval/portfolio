<nav id="admin-navbar" class="navbar navbar-expand-lg justify-content-center navbar-dark bg-dark">
    <div class="container">
        <div class="row">
            <a class="navbar-brand" href="{{ route('admin') }}">
                <img src="{{ asset('storage/images/logo-white.svg') }}" alt="Duval Gael" style="height: 25px;">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link px-4" href="{{ route('admin') }}"> Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link px-4" href="{{ route('admin-projects') }}"> Projects</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link px-4" href="{{ route('home') }}">View site</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link px-4" href="{{ route('logout') }}">Logout</a>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</nav>
