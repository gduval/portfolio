<nav>
    <ul class="nav justify-content-center">
        <li class="nav-item">
            <u>
                <a class="nav-link active" href="{{ route('home') }}">Projects</a>
            </u>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('about-me') }}">About me</a>
        </li>
        @auth()
            <li  class="nav-item">
                <a class="nav-link text-primary" href="{{ route('admin') }}">Back office</a>
            </li>
        @endauth
    </ul>
</nav>
