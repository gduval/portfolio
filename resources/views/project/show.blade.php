@php
    /** @var \App\Models\Project $project */
@endphp

@extends('layouts.app')

@section('title', $project->getTitle())
@section('pageDescription', $project->getTitle())

@section('content')
    <div id="project-content" class="container-fluid">
        <header class="row main-header header-black text-center px-5" style="background: url('{{ $project->getCover() }}');" >

           @include('components.nav', ['whiteNav' => true])

            <h2 class="w-100 my-5">
                <img src="{{ $project->getLogo() }}" alt="{{ $project->getTitle() }}" style="max-width: 250px; max-height: 250px;">
            </h2>
        </header>

        <article id="content" data-hover-style="text-small" class="pb-5 pt-2">
            {!! $project->getContent() !!}
        </article>

        @include('components.cta')

        @include('components.footer')
    </div>
@endsection
