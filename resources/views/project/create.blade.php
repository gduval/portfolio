@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row py-5">
            <form class="w-100" method="POST" action="{{ route('projects.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="form-group col-12">
                        <label for="project-title col-6">Title</label>
                        <input type="text" class="lg-form-input form-control @error('title') border-danger @enderror" name="title"
                               id="project-title"
                               aria-describedby="emailHelp" placeholder="Project title" value="{{ old('title') }}">
                        @error('title') <p class="text-danger">{{ $errors->first('title') }}</p> @enderror
                    </div>
                </div>

                <div class="form-row align-items-center">
                    <div class="form-group col-6">
                        <label for="type">Project type</label>
                        <select class="form-control" id="type" name="type" required>
                            <option value="" selected disabled hidden>Choose a type</option>
                            <option {{ old('type') === \App\Models\Project::TYPE_DEV ? 'selected' : ''}}>{{ \App\Models\Project::TYPE_DEV }}</option>
                            <option {{ old('type') === \App\Models\Project::TYPE_DESIGN ? 'selected' : ''}}>{{ \App\Models\Project::TYPE_DESIGN }}</option>
                        </select>
                    </div>
                    <div class="col ml-3">
                        <label for="draft">Is project Draft ?</label><br>
                        <input type="checkbox" name="draft" id="draft" />
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-6">
                        <label for="project-cover">Cover image</label><br>
                        <input type="file" class="form-control-file" id="cover" name="cover" value="{{ old('cover') }}">
                        @error('cover') <p class="text-danger">{{ $errors->first('cover') }}</p> @enderror
                    </div>
                    <div class="col-6">
                        <label for="project-cover">Logo</label><br>
                        <input type="file" class="form-control-file" id="logo" name="logo" value="{{ old('logo') }}">
                        @error('logo') <p class="text-danger">{{ $errors->first('logo') }}</p> @enderror
                    </div>
                </div>

                <div class="form-group">
                    <label for="project-content">Content</label>
                    <textarea id="editor" class="form-control @error('content') border-danger @enderror" name="content"
                              id="project-content">{{ old('content') }}</textarea>
                    @error('content') <p class="text-danger">{{ $errors->first('content') }}</p> @enderror
                </div>

                <button type="submit" class="btn btn-outline-dark" v-on:click="updateHiddenCategories"><i class="material-icons">done</i>
                    Save
                </button>
            </form>
        </div>
    </div>
@endsection
