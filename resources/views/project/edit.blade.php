@php
    /** @var \App\Models\Project $project */
@endphp

@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row py-5">
            <form class="w-100" method="POST" action="{{ route('projects.update', ['project' => $project]) }}"
                  enctype="multipart/form-data">
                @csrf

                @method('PUT')

                <div class="form-row">
                    <div class="form-group col-6">
                        <label for="project-title col-6">Title</label>
                        <input type="text" class="lg-form-input form-control @error('title') border-danger @enderror"
                               name="title"
                               id="project-title"
                               aria-describedby="emailHelp" placeholder="Project title"
                               value="{{ old('title') ?? $project->getTitle() }}">
                        @error('title') <p class="text-danger">{{ $errors->first('title') }}</p> @enderror
                    </div>
                </div>

                <div class="form-row align-items-center">
                    <div class="form-group col-6">
                        <label for="exampleFormControlSelect1">Project type</label>
                        <select class="form-control" id="type" name="type"
                                value="{{ old('type') ?? $project->getType() }}" required>
                            <option value="" selected disabled hidden>Choose a type</option>
                            <option value="{{ \App\Models\Project::TYPE_DEV }}"
                                {{ $project->getType() === \App\Models\Project::TYPE_DEV ? 'selected' : ''}}>
                                {{ \App\Models\Project::TYPE_DEV }}
                            </option>
                            <option value="{{ \App\Models\Project::TYPE_DESIGN }}"
                                {{ $project->getType() === \App\Models\Project::TYPE_DESIGN ? 'selected' : ''}}>
                                {{ \App\Models\Project::TYPE_DESIGN }}
                            </option>
                        </select>
                    </div>
                    <div class="col ml-3">
                        <label for="draft">Is project Draft ?</label><br>
                        <input type="checkbox" name="draft" id="draft" {{ ($project->isDraft()) ? 'checked' : '' }}/>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="project-cover">Cover image</label>
                            <div class="mb-3">
                                <img src="{{ asset($project->getCover()) }}" alt=""
                                     style="width: 200px; border-radius: 10px; box-shadow: 0 0 10px RGBa(0,0,0,.2)">
                            </div>
                            <input type="file" class="form-control-file" id="cover" name="cover"
                                   value="{{ old('cover') }}">
                            @error('cover') <p class="text-danger">{{ $errors->first('cover') }}</p> @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="project-cover">Project logo</label>
                            <div class="mb-3">
                                <div
                                    style="height: 150px; width: 250px; position: relative; background: url('{{ asset($project->getCover()) }}'); background-size: cover;">
                                    <img src="{{ asset($project->getLogo()) }}"
                                         style="width: 200px; border-radius: 10px; box-shadow: 0 0 10px RGBa(0,0,0,.2); position: absolute; top: 50%; left: 50%; transform: translateY(-50%) translateX(-50%); max-height: 125px; max-width: 125px;">
                                </div>
                            </div>
                            <input type="file" class="form-control-file" id="logo" name="logo"
                                   value="{{ old('logo') }}">
                            @error('logo') <p class="text-danger">{{ $errors->first('logo') }}</p> @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col my-5">
                        <button type="submit" class="btn btn-success" v-on:click="updateHiddenCategories">Save</button>
                    </div>
                </div>

                <div class="form-group">
                    <label for="project-content">Content</label>
                    <textarea id="editor" class="form-control @error('content') border-danger @enderror" name="content"
                              id="project-content">{!! old('content') ?? $project->getContent() !!}</textarea>
                    @error('content') <p class="text-danger">{{ $errors->first('content') }}</p> @enderror
                </div>

                <button type="submit" class="btn btn-success" v-on:click="updateHiddenCategories">Save</button>
            </form>
        </div>
    </div>


    <script>
        initProjectType()
    </script>
@endsection
