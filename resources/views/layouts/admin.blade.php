<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">

    <!-- Favicon -->
    <link rel="icon" href="{{ asset('storage/images/favicon.ico') }}" />
</head>
<body class="h-100">

<div id="app" class="h-100">
    @include('nav.admin-nav')

    @yield('content')
</div>

{{-- Script --}}
<script src="{{ asset('js/admin.js') }}"></script>
@yield('scripts')
</body>
</html>
