<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('components.doggo')

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114391781-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-114391781-1');
    </script>


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0"/>
    <meta name="description"
          content="@yield('pageDescription', "Hey, I am Gaël Duval, a freelance UI / UX Designer and Full Stack developer based in Paris !")">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link rel="preload" href="{{ asset('fonts/raisonne/raisonne.woff2') }}" as="font" crossorigin="anonymous" type="font/woff2" />

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('storage/images/favicons/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href=""{{ asset('storage/images/favicons/favicon-32x32.png') }}>
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('storage/images/favicons/favicon-16x16.png') }}">
</head>

<body id="portfolio-body" autofocus>

@include('components.accessible-nav')

{{-- Lerp cursor --}}
<div id="circle" class="d-none d-lg-block" style="opacity:0">
    <div id="cursor-bg"></div>
</div>

@include('components.responsive-nav-content')

<main id="app" class="pt-lg-4 px-lg-4 bg-white">
    @include('components.page-transition-overlay')

    @yield('content')
</main>

{{-- Script --}}
<script type="module" src="{{ asset('js/app.js') }}" rel=preload></script>
<script type="module" src="{{ asset('js/responsive-nav.js') }}" ></script>

</body>
</html>
