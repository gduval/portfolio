@extends('layouts.app')

@section('title', 'Home')

@section('content')
    <div class="container h-100">
        <div class="row h-100">
            <div class="my-auto col-lg-6 col-xl-6 col-md-8 col-sm-10 col-xs-10 mx-auto text-center mt-5">
                <div id="nav" class="container mb-5">
                    @include('nav.nav')
                </div>

                <h1 class="mb-5"><img id="logo" src=" {{ asset('storage/images/logo.png') }}" alt="Gaël Duval logotype"/></h1>

                <p class="h2">
                    Hey ! My name is Gaël Duval, i'm a freelance <u>Full Stack Developer</u> and <u>UI/UX Designer</u> !
                </p>

                <ul id="external-links" class="mt-5">
                    <li><a class="mr-2" href="https://www.linkedin.com/in/ga%C3%ABl-duval-a0ab41b7"
                           target="_blank">LinkedIn</a></li>
                    •
                    <li><a class="mr-2 ml-2" href="https://dribbble.com/Liliaroth" target="_blank">Dribbble</a></li>
                    •
                    <li><a class="mr-2 ml-2" href="mailto:gduval.adm@gmail.com" target="_blank">Email</a></li>
                </ul>

                <i class="material-icons mt-5" style="font-size: 2em">
                    keyboard_arrow_down
                </i>
            </div>
        </div>
    </div>

    {{-- Projects --}}
    <x-project/>
@endsection
