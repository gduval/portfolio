@php
    /** @var \Illuminate\Database\Eloquent\Collection|\App\Models\Project[] $projects */

@endphp

@extends('layouts.app')

@section('title', 'Home')

@section('pageDescription', "Hi, i am Duval Gael ! Learn more about the projects I worked on, from website redesign to complex web application.")

@section('content')
    {{-- Rotating Spheres --}}
    <div class="rotating-sphere-container">
        <img id="rotating-sphere" src="{{ asset('storage/images/rotating-sphere-min.png') }}" alt="Rotating Sphere"
             title="Rotating Sphere" loading="eager">
    </div>

    <section class="container-fluid">
        <header class="row main-header text-center justify-content-center">

            @include('components.nav')

            <h2 data-hover-style="text" class="mt-5 col-sm-12 col-lg-8">
                Hi ! I'm <strong>Duval Gaël</strong>,<br>
                Welcome to my portfolio !
            </h2>

        </header>
    </section>
    <section id="content" class="container">
        <section class="row position-relative justify-content-center">
            <aside class="col-sm-12 col-lg-3">
                <p class="horizontal-title position-relative">
                    Projects
                </p>
            </aside>
            <div class="col-sm-12 col-lg-6">
                <h3 data-hover-style="text-small">
                    Learn more about the projects I worked on, from <strong data-hover-style="text-small">website
                        redesign to complex web application</strong>.
                </h3>
            </div>
            <div class="col-sm-12 col-lg-3">
                <a href="{{ route('about-me') }}"
                   class="btn btn-outline-dark mt-2" data-hover-style="dot">Learn about me</a>
            </div>
        </section>
    </section>
    <section class="container-fluid">
        <section class="container-md projects-list pb-5">
            @foreach($projects->chunk(3) as $groupedProjects)
                @switch ($groupedProjects->count())
                    @case(1)
                    @include('components.one-article-layout', ['projects' => $groupedProjects])
                    @break
                    @case(2)
                    @include('components.two-articles-layout', ['projects' => $groupedProjects])
                    @break
                    @case(3)
                    @include('components.three-articles-layout', ['projects' => $groupedProjects, 'reversed' => $loop->odd])
                    @break
                    @default
                    @break
                @endswitch
            @endforeach
        </section>

        @include('components.clients')

        @include('components.cta')

        @include('components.footer')

    </section>
@endsection
