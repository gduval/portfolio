@php
    /** @var \App\Models\Project $project */
@endphp

@foreach($projects() as $project)
    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 mb-4">
        <div class="card w-100">
            <div class="card-img-top div-card-top" style="background: url('{{ $project->getCover() }}');"></div>
            <div class="card-body">
                <h5 class="card-title">{{ $project->getTitle() }}</h5>
{{--                <p>--}}
{{--                    @foreach($project->getCategoriesArray() as $category)--}}
{{--                        <span class="badge badge-dark">{{ $category }}</span>--}}
{{--                    @endforeach--}}
{{--                </p>--}}
                <a href="{{ route('projects.show', $project) }}" class="btn btn-light">View</a>
                <a href="{{ route('projects.edit', $project) }}" class="btn btn-light">Edit</a>
                <a href="#" class="btn btn-outline-danger" v-on:click="deleteArticle('{{ route('projects.destroy', $project) }}')">Delete</a>
            </div>
        </div>
    </div>
@endforeach
