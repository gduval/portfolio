<nav class="d-flex {{ isset($whiteNav) ? 'white-nav' : '' }}" >
    <h1>
        <a href="{{ route('home') }}" title="Go to Home page">
            <img data-hover-style="dot"
                 src="{{ isset($whiteNav) ? asset('storage/images/logo-white.svg') : asset('storage/images/logo.svg' )}}"
                 alt="Duval Gaël">
        </a>
    </h1>

    <div class="d-inline d-lg-none">
        <div id="nav-circle-background"></div>

        <!-- Full Credit to https://codepen.io/ainalem for his animated hamburger menu icon. -->
        <button data-hover-style="dot" id="responsive-nav-button" class="menu"
                onclick="this.classList.toggle('opened');this.setAttribute('aria-expanded', this.classList.contains('opened'))"
                aria-label="Main Menu">
            <svg width="50" height="50" viewBox="0 0 100 100">
                <path class="line line1"
                      d="M 20,29.000046 H 80.000231 C 80.000231,29.000046 94.498839,28.817352 94.532987,66.711331 94.543142,77.980673 90.966081,81.670246 85.259173,81.668997 79.552261,81.667751 75.000211,74.999942 75.000211,74.999942 L 25.000021,25.000058"/>
                <path class="line line2" d="M 20,50 H 80"/>
                <path class="line line3"
                      d="M 20,70.999954 H 80.000231 C 80.000231,70.999954 94.498839,71.182648 94.532987,33.288669 94.543142,22.019327 90.966081,18.329754 85.259173,18.331003 79.552261,18.332249 75.000211,25.000058 75.000211,25.000058 L 25.000021,74.999942"/>
            </svg>
        </button>
    </div>

    <ul class="d-none d-lg-inline">
        @if (\Illuminate\Support\Facades\Auth::user())
            <li data-hover-style="dot">
                <a data-hover-style="dot" href="{{ route('admin-projects') }}" class="text-primary">Admin</a>
            </li>
        @endif
        <li data-hover-style="dot">
            <a
                data-hover-style="dot"
                class="{{ request()->routeIs(['home', 'projects.show']) ? 'active' : '' }}"
                href="{{ route('home') }}"
            >
                Projects
            </a>
        </li>
        <li data-hover-style="dot">
            <a
                data-hover-style="dot"
                class="{{ request()->routeIs('about-me') ? 'active' : '' }}"
                href="{{ route('about-me') }}"
            >
                About me
            </a>
        </li>
        <li class="dropdown-list-container" data-hover-style="dot" >
            <button
                class="btn dropdown-btn download-cv {{ isset($whiteNav) ? 'btn-outline-light ' : 'btn-outline-dark' }}">
                Download my Resume
            </button>

            <article class="dropdown-container">
                <a class="dropdown-link-item" href="{{ asset('storage/images/Resume/Duval-Gael-EN.pdf') }}"
                   target="_blank" rel="noopener" style="opacity:0">English</a>
                <a class="dropdown-link-item" href="{{ asset('storage/images/Resume/Duval-Gael-FR.pdf') }}"
                   target="_blank" rel="noopener" style="opacity:0">French</a>
            </article>
        </li>
    </ul>
</nav>
