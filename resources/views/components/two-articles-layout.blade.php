<div class="projects-row row no-gutters">
    <div
        class="col-sm-12 col-lg-6">
        <a class="projects-row" href="{{ route('projects.show', $projects->first()->getSlug()) }}">
            <article data-hover-style="dot" class="project-card"
                     style="background: url({{ $projects->first()->getCover() }});">
                <div class="article-gradient-overlay"></div>
                <h4>
                    <img data-hover-style="dot" src="{{ $projects->first()->getLogo()  }}"
                         alt="{{  $projects->first()->getTitle() }}" loading="lazy">
                </h4>
            </article>
        </a>
    </div>
    <div
        class="col-sm-12 col-lg-6">
        <a class="projects-row" href="{{ route('projects.show', $projects->last()->getSlug()) }}">
            <article data-hover-style="dot" class="project-card"
                     style="background: url({{ $projects->last()->getCover() }});">
                <div class="article-gradient-overlay"></div>
                <h4>
                    <img data-hover-style="dot" src="{{ $projects->last()->getLogo()  }}"
                         alt="{{  $projects->last()->getTitle() }}" loading="lazy">
                </h4>
            </article>
        </a>
    </div>
</div>
