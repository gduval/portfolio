<aside id="call-to-action-container" class="container-md" role="complementary">
    <div class="row">
        <div class="col-xs-0 col-lg-6"></div>
        <div id="call-to-action" class="col-12">
            <div class="row">
                <div class="col-xs-12 col-lg-4">
                    <img id="blue-birds-image" class="d-none d-lg-inline"
                         src="{{ asset('storage/images/blue-birds.png') }}" alt="Blue Birds" title="Blue Birds"
                         loading="lazy">
                </div>
                <div class="col-xs-12 col-lg-8">
                    <h3 data-hover-style="text-small">
                        Working on a project and looking for a freelance FullStack Developer or UI • UX Designer ? <a
                            data-hover-style="dot" href="mailto: gduval.adm@gmail.com"><u>Contact
                                me</u></a>, or
                        <a data-hover-style="dot" href="{{ asset('storage/images/Resume/Duval-Gael-EN.pdf') }}"
                           target="_blank">
                            <u>Download my Resume</u>
                        </a>.
                    </h3>
                    <h4 data-hover-style="text-small" class="mt-3">
                        <i>French ? Don't worry, here is my
                            <a data-hover-style="dot" href="{{ asset('storage/images/Resume/Duval-Gael-FR.pdf') }}"
                            target="_blank">Resume in French</a> 🥐 !
                        </i>
                    </h4>
                </div>
            </div>
        </div>
    </div>
</aside>
