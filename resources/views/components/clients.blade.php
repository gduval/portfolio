<aside id="clients" class="container justify-content-center mt-5" role="complementary">
    <div class="row justify-content-center">
        <div class="col-sm-12 col-lg-6">
            <p data-hover-style="text-small" class="horizontal-title position-relative text-center">
                Clients
            </p>
        </div>
    </div>
    <div class="row position-relative justify-content-center">
        <div data-hover-style="text-small" class="col-sm-12 col-lg-6">
            <h3 class="text-center">Here are some amazing companies I had the opportunity to work with.</h3>
        </div>
    </div>
    <section class="row justify-content-center py-5">
        <a href="https://www.artefact.com" target="_blank" rel="noopener">
            <article data-hover-style="dot" class="client-box">
                <img src="{{ asset('storage/images/clients/artefact.png') }}" alt="Artefact" title="Artefact"
                     loading="lazy">
            </article>
        </a>
        <a href="https://www.tracknlocate.io" target="_blank" rel="noopener">
            <article data-hover-style="dot" class="client-box">
                <img src="{{ asset('storage/images/clients/tracknlocate.png') }}" alt="Track & Locate"
                     title="Track & Locate" loading="lazy">
            </article>
        </a>
        <a href="https://www.osb-communication.com/" target="_blank" rel="noopener">
            <article data-hover-style="dot" class="client-box">
                <img src="{{ asset('storage/images/clients/osb-communication.png') }}" alt="OSB Communication"
                     title="OSB Communication" loading="lazy">
            </article>
        </a>
        <a href="https://www.digitalseeder.com/" target="_blank" rel="noopener">
            <article data-hover-style="dot" class="client-box">
                <img src="{{ asset('storage/images/clients/digital-seeder.png') }}" alt="Digital Seeder"
                     title="Digital Seeder" loading="lazy">
            </article>
        </a>
        <a href="http://www.nouas.org" class="d-none d-sm-inline-block" target="_blank" rel="noopener">
            <article data-hover-style="dot" class="client-box">
                <img src="{{ asset('storage/images/clients/nouas.png') }}" alt="Nouas Formation" title="Nouas Formation"
                     loading="lazy">
            </article>
        </a>
        <a href="https://wordpress-freelance.com/" class="d-none d-sm-inline-block" target="_blank" rel="noopener">
            <article data-hover-style="dot" class="client-box">
                <img src="{{ asset('storage/images/clients/webstrategy.png') }}" alt="WebStrategy" title="WebStrategy"
                     loading="lazy">
            </article>
        </a>
    </section>
</aside>
