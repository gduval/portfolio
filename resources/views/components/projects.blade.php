@php
    /** @var \App\Models\Project $project */
@endphp

@foreach($projects() as $project)
    <div class="container-fluid h-100">
        <a href="{{ route('projects.show', ['project' => $project]) }}" class="row h-100 bg-dark articles position-relative">
            <div class="col-sm-12 my-auto text-center text-white position-relative" style="z-index: 2">
                <small>{{ $project->getType() }}</small>
                <h2 class="mb-3 h2">{{ $project->getTitle() }}</h2>

                <ul>
                    @foreach($project->getCategoriesArray() as $category)
                        <li>{{ $category }}</li>
                    @endforeach
                </ul>

                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
            </div>

            <div class="article-cover h-100 w-100 position-absolute"
                 style="background-image: url('{{ $project->getCover() }}');">
                <div class="h-100 w-100 cover-overlay"></div>
            </div>
        </a>
    </div>
@endforeach

