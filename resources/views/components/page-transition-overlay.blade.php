<style>
    #white-overlay {
        float: right;
        width: 100%;
        height: 100%;
        background: white;
        position: fixed;
        top: 0;
        right: 0;
        z-index: 100;
        transition: width 1s;
    }
</style>
<div id="white-overlay"></div>
