<footer id="footer" class="container-sm">
    <section class="text-center">
        <a data-hover-style="dot" class="p-3" href="https://www.linkedin.com/in/ga%C3%ABl-duval-a0ab41b7/"
           target="_blank" rel="noopener">
            <img src="{{ asset('storage/images/linkedin.svg') }}"
                 alt="Follow me on Linkedin">
        </a>
        <a data-hover-style="dot" class="p-3" href="https://dribbble.com/Liliaroth" target="_blank" rel="noopener">
            <img src="{{ asset('storage/images/dribbble.svg') }}"
                 alt="Follow me on Dribbble">
        </a>
    </section>
</footer>
