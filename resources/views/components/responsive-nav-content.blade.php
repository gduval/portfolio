<section id="responsive-nav-container" aria-label="Responsive navigation" style="opacity:0;">
    <ul>
        <li class="responsive-nav-item text-center mt-0">
            <a href="{{ route('home') }}">Projects</a>
        </li>
        <li class="responsive-nav-item text-center">
            <a href="{{ route('about-me') }}">About Me</a>
        </li>
    </ul>

    <div id="responsive-nav-separator"></div>

    <footer class="text-center">
        <ul>
            <li class="responsive-nav-item text-center">
                <a href="{{ asset('storage/images/Resume/Duval-Gael-EN.pdf') }}" target="_blank" rel="noopener"
                   class="outline">Resume - EN</a>
            </li>
            <li class="responsive-nav-item text-center">
                <a href="{{ asset('storage/images/Resume/Duval-Gael-FR.pdf') }}" target="_blank" rel="noopener"
                   class="outline">Resume - FR</a>
            </li>
        </ul>

        <a data-hover-style="dot" href="https://liliaroth.dribbble.com/" class="p-2" target="_blank" rel="noopener">
            <svg shape-rendering="geometricPrecision" class="responsive-nav-item" xmlns="http://www.w3.org/2000/svg"
                 aria-label="Dribbble logo"
                 width="25" height="25"
                 viewBox="0 0 23.781 23.781">
                <path shape-rendering="geometricPrecision"
                      d="M12.039.148a11.454,11.454,0,0,0-8.4,3.489,11.449,11.449,0,0,0-3.489,8.4,11.455,11.455,0,0,0,3.49,8.4,11.451,11.451,0,0,0,8.4,3.489,11.452,11.452,0,0,0,8.4-3.49,11.449,11.449,0,0,0,3.489-8.4,11.458,11.458,0,0,0-3.49-8.4A11.444,11.444,0,0,0,12.039.148ZM19.328,6.3a8.527,8.527,0,0,1,1.964,5.325,16.83,16.83,0,0,0-3.774-.465,14.759,14.759,0,0,0-2.688.259q-.207-.517-.62-1.448A14.883,14.883,0,0,0,19.328,6.3Zm-7.29-3.568A9.034,9.034,0,0,1,17.829,4.8a12.087,12.087,0,0,1-4.55,3.257,36.546,36.546,0,0,0-3.205-5.067,8.1,8.1,0,0,1,1.964-.259Zm-4.084.983a32.03,32.03,0,0,1,3.308,5.067A26.241,26.241,0,0,1,3.922,9.816H3.767a3.415,3.415,0,0,1-.724-.052A8.689,8.689,0,0,1,7.954,3.715ZM2.733,12.039v-.155a6.482,6.482,0,0,0,1.034.052,26.9,26.9,0,0,0,8.427-1.24q.1.207.285.62t.285.62a14.622,14.622,0,0,0-5.274,3.1,13.014,13.014,0,0,0-2.533,3,9.047,9.047,0,0,1-2.223-6Zm9.306,9.306a8.855,8.855,0,0,1-5.531-1.861,17.378,17.378,0,0,1,2.171-2.636A12.614,12.614,0,0,1,13.486,13.9a48.385,48.385,0,0,1,1.861,6.824,8.827,8.827,0,0,1-3.308.62Zm5.274-1.655a52.539,52.539,0,0,0-1.706-6.255,10.719,10.719,0,0,1,1.861-.155h.052a15.786,15.786,0,0,1,3.619.465,8.9,8.9,0,0,1-3.826,5.946Z"
                      transform="translate(-0.148 -0.148)" fill="#ffffff"/>
            </svg>
        </a>
        <a data-hover-style="dot" href="https://www.linkedin.com/in/ga%C3%ABl-duval-a0ab41b7/" target="_blank"
           class="p-2" rel="noopener">
            <svg shape-rendering="geometricPrecision" class="responsive-nav-item" xmlns="http://www.w3.org/2000/svg"
                 aria-label="LinkedIn logo"
                 width="25" height="25" viewBox="0 0 19.153 18.309">
                <path shape-rendering="geometricPrecision"
                      d="M4.348,18.309V5.956H.242V18.309ZM2.3,4.268A2.141,2.141,0,1,0,2.323,0a2.14,2.14,0,1,0-.054,4.268H2.3ZM6.62,18.309h4.106v-6.9a2.81,2.81,0,0,1,.135-1,2.247,2.247,0,0,1,2.107-1.5c1.486,0,2.08,1.133,2.08,2.793v6.609h4.106V11.225c0-3.794-2.026-5.56-4.727-5.56A4.089,4.089,0,0,0,10.7,7.747h.028V5.955H6.621c.054,1.159,0,12.353,0,12.353Z"
                      transform="translate(0)" fill="#ffffff"/>
            </svg>

        </a>
    </footer>
</section>
