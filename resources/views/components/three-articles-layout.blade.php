<div class="projects-row row no-gutters">
    <div
        class="{{ $reversed ? 'order-last' : 'order-first' }} col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <a class="projects-row" href="{{ route('projects.show', $projects->first()->getSlug()) }}">
            <article data-hover-style="dot" class="project-card"
                     style="background: url({{ $projects->first()->getCover() }});">
                <div class="article-gradient-overlay"></div>
                <h4>
                    <img data-hover-style="dot" src="{{ $projects->first()->getLogo()  }}"
                         alt="{{  $projects->first()->getTitle() }}" loading="lazy">
                </h4>
            </article>
        </a>
    </div>
    @php
        $secondProject = $projects->slice(1,1)->first();
        $thirdProject = $projects->slice(2, 1)->first();
    @endphp
    <div
        class="{{ $reversed ? 'order-first' : 'order-last' }} col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <a href="{{ route('projects.show', $secondProject->getSlug()) }} ">
            <article data-hover-style="dot" class="project-card project-card-small"
                     style="background: url({{ $secondProject->getCover() }})">
                <div class="article-gradient-overlay"></div>
                <h4>
                    <img data-hover-style="dot" src="{{ $secondProject->getLogo() }}"
                         alt="{{ $secondProject->getTitle() }}" loading="lazy">
                </h4>
            </article>
        </a>
        <a href="{{ route('projects.show', $thirdProject->getSlug()) }}">
            <article data-hover-style="dot" class="project-card project-card-small"
                     style="background: url({{ $thirdProject->getCover() }})">
                <div class="article-gradient-overlay"></div>
                <h4>
                    <img data-hover-style="dot" src="{{ $thirdProject->getLogo() }}"
                         alt="{{ $thirdProject->getTitle() }}" loading="lazy">
                </h4>
            </article>
        </a>
    </div>
</div>
