<div class="projects-row row no-gutters">
    <div
        class="col-12">
        <a class="projects-row" href="{{ route('projects.show', $projects->first()->getSlug()) }}">
            <article data-hover-style="dot" class="project-card"
                     style="background: url({{ $projects->first()->getCover() }});">
                <div class="article-gradient-overlay"></div>
                <h4>
                    <img data-hover-style="dot" src="{{ $projects->first()->getLogo()  }}"
                         alt="{{  $projects->first()->getTitle() }}" loading="lazy">
                </h4>
            </article>
        </a>
    </div>
</div>
