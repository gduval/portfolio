@php
    /** @var \Illuminate\Database\Eloquent\Collection|\App\Models\Project[] $projects */
@endphp

@extends('layouts.app')

@section('title', 'About Me')
@section('pageDescription', "Hi, i am Duval Gael ! I am a freelance UI / UX Designer and Full Stack developer based in Paris.")

@section('content')
    <div class="container-fluid">
        <header class="row main-header header-purple text-center justify-content-center mb-5">

            @include('components.nav', ['whiteNav' => true])

            <h2 data-hover-style="text" class="col-sm-12 col-lg-8">
                Hi ! I'm Duval Gaël 🍵
            </h2>
            <h3 data-hover-style="text-small" class="col-sm-12 col-lg-8">I'm a freelance UI • UX Designer and Full Stack
                developer
                based in Paris.</h3>
        </header>
    </div>
    <section id="content" class="container pt-5">
        <div class="row position-relative justify-content-center">
            <div class="col-sm-12 col-lg-3">
                <p data-hover-style="text-small" class="horizontal-title position-relative">
                    About me
                </p>
            </div>
            <div data-hover-style="text-small" class="col-sm-12 col-lg-6">
                <h3>I love working closely with companies and creating products they <i>- and I -</i> will be proud of.
                </h3>
            </div>
            <div class="col-sm-12 col-lg-3 mb-sm-5">
                <a href="{{ route('home') }}" class="btn btn-outline-dark mt-2" data-hover-style="dot">See my
                    projects</a>
            </div>
        </div>
        <div class="row justify-content-center">
            <p data-hover-style="text-small" class="col-sm-12 col-lg-6 py-5 my-5">
                Hey 👋 I'm Gaël Duval, freelance Fullstack Developer and UI - UX Designer from France (Paris) ! I
                studied in 42 School Paris and <b>have been a developer and UI - UX Designer for more than 3
                    years</b>. I had
                the opportunity to work on several <i>- and very different -</i> projects, such as a <b>Social
                    Network mobile app</b> (as
                UI - UX Designer), a complex <b>Assets Tracking System</b> (as a Laravel / Python Developer),
                and <b>much more</b>.
            </p>
        </div>
    </section>
    <section class="container py-5">
        <div data-hover-style="text-small" class="row position-relative">
            <p class="col-sm-12 col-lg-3 horizontal-title position-relative">
                My Skills
            </p>
            <h4 data-hover-style="text-small" class="col-sm-12 col-lg-3">
                Development
            </h4>
            <p class="col-sm-12 col-lg-4 mb-sm-5">
                <b>Laravel 7+</b>, <b>PHP 7+</b>, <b>Javascript ES6</b>, VueJS, <b>JQuery</b>, Sass, <b>Bootstrap</b>,
                Neo4j.
            </p>
        </div>
        <div data-hover-style="text-small" class="row position-relative">
            <p class="col-sm-12 col-lg-3"></p>
            <h4 data-hover-style="text-small" class="col-sm-12 col-lg-3">
                UI - UX Design
            </h4>
            <p class="col-sm-12 col-lg-4 mb-sm-5">
                <b>Adobe Xd</b>, <b>Adobe Photoshop</b>, Zeplin.
            </p>
        </div>
        <div data-hover-style="text-small" class="row position-relative">
            <p class="col-sm-12 col-lg-3"></p>
            <h4 data-hover-style="text-small" class="col-sm-12 col-lg-3">
                ... and more
            </h4>
            <p class="col-sm-12 col-lg-4 mb-sm-5">
                Wordpress, <b>Git</b>, <b>Gitlab</b>, Docker, Ansible, SSH-SCP, Database Replication.
            </p>
        </div>
    </section>

    <section class="container py-5" aria-labelledby="My Soft Skills" aria-describedby="info">
        <div data-hover-style="text-small" class="row position-relative">
            <p class="col-sm-12 col-lg-3 horizontal-title position-relative">
                My Soft Skills
            </p>
            <div data-hover-style="text-small" class="col-sm-12 col-lg-8">
                <article aria-labelledby="Pressure management" aria-describedby="info">
                    <h4>I know how to handle pressure 💪</h4>

                    <p>
                        I have experience working on ambitious projects, with short deadlines in pressure situations
                        and this is my comfort zone !
                    </p>
                </article>

                <article aria-labelledby="calendar" aria-describedby="info">
                    <h4>I 💜 peer learning</h4>

                    <p>
                        As we all have different experiences, specialties and opinions, exchanging within a team is what
                        pulls us up, and i'm 100% in !
                    </p>
                </article>

                <article aria-labelledby="calendar" aria-describedby="info">
                    <h4>I am creative 🦄</h4>

                    <p>
                        I love to bring creativity and perfectionism to my work. I guess you can get a little glimpse of
                        it by visiting my
                        <a data-hover-style="dot"
                           href="https://dribbble.com/Liliaroth"
                           title="Visit my Dribbble profile !"
                           target="_blank"
                           rel="noopener">Dribbble Portfolio</a> !
                    </p>
                </article>
            </div>
        </div>
    </section>

    @include('components.clients')

    <section class="container client-citation py-5 my-5">
        <figure class="col-sm-12 col-lg-8 m-auto justify-content-center">
            <div class="citation-client green-client m-auto">
                <img class="d-block" src="{{ asset('storage/images/clients/michel-mendy.png') }}" alt="Michel Mendy"
                     loading="lazy"/>
            </div>
            <blockquote data-hover-style="text" cite="https://www.linkedin.com/in/michel-mendy-55a003111/"
                        class="pt-4 text-center">
                <p>Gaël does an excellent job as a UX designer for my startup. He is very serious, involved and brings a
                    creative touch to my project. I highly recommend him.</p>
            </blockquote>
            <figcaption data-hover-style="text-small" class="text-center">Michel Mendy, Co-founder at BillionKeys, via
                <a data-hover-style="dot" href="https://www.linkedin.com/in/ga%C3%ABl-duval-a0ab41b7/" target="_blank"
                   rel="noopener">LinkedIn</a>.
            </figcaption>
        </figure>
    </section>

    @include('components.cta')

    @include('components.footer')

    </div>
@endsection
